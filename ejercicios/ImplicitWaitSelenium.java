package org.sele;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ImplicitWaitSelenium {
	
	//declaramos el web driver y la url a testear
	private WebDriver driver;
	private String baseUrl;
	
	@Before
	public void SetUp() throws Exception {
		//seleccionamos el web driver para el explorador que queremmos
		driver = new FirefoxDriver();
		baseUrl = "http://letskodeit.teachable.com/pages/practice";
		
		//comando para maximizar la ventana
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(3,TimeUnit.SECONDS);
	}

	@Test
	public void test() throws Exception {
		driver.get(baseUrl);
		// selecciona el texto de login 
		driver.findElement(By.linkText("Login")).click();
		// escribe test dentro del cuadro user email 
		driver.findElement(By.id("user_email")).sendKeys("test");
	}
	
	@After
	public void tearDown() throws Exception {
		Thread.sleep(3000);
		driver.quit();
	}
}
