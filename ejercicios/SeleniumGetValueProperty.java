package org.sele;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SeleniumGetValueProperty {

	private WebDriver driver;
	private String baseURL;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		driver.get(baseURL);
		
		WebElement element = driver.findElement(By.id("name"));
		String attributeValue = element.getAttribute("class");
		
		System.out.println("the value of the attribute is: " + attributeValue);
		
	}
	
	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		driver.quit();
		
	}

}
