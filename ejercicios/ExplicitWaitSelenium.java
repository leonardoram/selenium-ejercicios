package org.sele;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ExplicitWaitSelenium {
	private WebDriver driver;
	private String baseUrl;
	WaitTypes wt;

	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://letskodeit.teachable.com/pages/practice";
		wt = new WaitTypes(driver);
		
		driver.manage().window().maximize();
	}

	@After
	public void tearDown() throws Exception {
		Thread.sleep(3000);
		driver.quit();
		
	}

	@Test
	public void test() {
		driver.get(baseUrl);
		webElement loginLink = driver.findElement(By.linkText("Login"));
		loginLink.click();
		//creamos el explicit wait con un webdriverwait 
		WebElement emailfield = wt.waitForElement(By.id("user_email"),3);
		emailField.sendKeys("test");
		
		driver.findElement(By.id("user_email")).sendKeys("test");
		
	}

}
