package org.sele;

public class GenericMethods {
	
	WebDriver driver;
	
	public GenericMethods(WebDriver driver){
		this.driver = driver;
		
	}

	public webElement getElement(String locator, String type) {
		type = type.toLowerCase();
		if(type.equals("id")) {
			System.out.println("element found with id: " +type);
			return this.driver.findElement(By.id(locator));
		}
		else if(type.equals("xpath")){
			System.out.println("element found with xpath: "+ type);
			return this.driver.findElement(By.xpath(locator));
		}
		else {
			System.out.println("locator find not supported");
			return null;
		}
	}
}
