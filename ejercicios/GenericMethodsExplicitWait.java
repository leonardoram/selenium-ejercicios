package org.sele;

public class GenericMethodsExplicitWait {
	
	WebDriver driver;
	
	public waitTypes(WebDriver driver){
		this.driver = driver;
	}
	
	public WebElement waitForElement(By locator, int timeout) {
		WebElement element = null;
		try {
			System.out.println("Waiting for maxx: "+timeout+ " to element to be available");
			WebDriverWait wait = new WebDriverWait(driver,timeout);
			element = wait.until(
					ExpectedConditions.visibilityOfElementLocated(locator));
			System.out.println("element appeared on the web page");
		} catch (Exception e) {
			System.out.println("element not appeared in the web page ");
		}
		
		return element;
	}

}
