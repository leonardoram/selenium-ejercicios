package org.sele;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import javax.xml.soap.Text;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SeleniumPractice {
	
	WebDriver driver;
	String baseUrl1;
	String baseUrl2;
	
	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl1 = "http://letskodeit.teachable.com/pages/practice";
		baseUrl2 = "http://www.expedia.com";
		driver.manage().timeouts().implicityWait(10,TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

	@Test
	public void testLetsKodeIt() throws InterruptedException {
		driver.get(baseUrl1);
		
		WebElement textBox = driver.findElement(By.id("displayed-text"));
		System.out.println("text box displayed: "+textBox.isDisplayed());
		
		Thread.sleep(2000);
		
		WebElement hideButton = driver.findElement(By.id("hide-textbox"));
		hideButton.click();
		System.out.println("clicked hide button");
		System.out.println("text box displayed"+textBox.isDisplayed());
		
		// show text box 
		Thread.sleep(3000);
		
		WebElement showButton = driver.findElement(By.id("show-textbox"));
		showButton.click();
		System.out.println("clicked show button");
		System.out.println("text box displayed"+textBox.isDisplayed());
	}
	
	@Test
	public void testExpedia() throws InterruptedException {
		driver.get(baseUrl2);
		WebElement childDropdown = driver.findElement(By.id("package-1-age-select-1"));
		System.out.println("child dropdown displayed " + childDropdown.isDisplayed());
	}
	
	@After
	public void tearDown() throws Exception {
		Thread.sleep(2000);
		driver.quit();
	}

}
